const Menus = require('../db/models/menuSchema')
const Role = require('../db/models/roleSchema')
const Counter = require('../db/models/counterSchema')
const { successModel, errorModel, code } = require("../utils/stateModel")
const { getMenuTree, delMenuTree } = require('../utils/operatMenu')
const jwt = require("jsonwebtoken")
const SECRET = process.env.JWT_SECRET
//查询菜单列表

const queryMenus = async (ctx, params) => {
    //从token中获取用户列表
    const { userList } = jwt.verify(ctx.headers.token, SECRET)
    //查询
    const resRole = await Role.find({ _id: { $in: userList.roleNames } })
    let menuIds = []
    resRole.forEach((item) => {
        menuIds = menuIds.concat(item.permId)
    })
    let { menuType, _ids, menuNum } = params
    if (_ids) {
        _ids = JSON.parse(_ids)
    }
    let selectParms = {}
    let conditionParms = {}
    if (menuType == 1) {
        selectParms.menuType = 1
    }
    //如果用户名为admin 默认获取全部菜单权限
    if (menuNum || userList.username == 'admin') {
        if (userList.username == 'admin') {
            //创建菜单管理菜单 便于创建后续菜单,运行一次后可注释掉  
            const exitMenu = await Menus.findOne({ name: '菜单管理' })
            if (!exitMenu) {
                await Menus.create({
                    id: 0,
                    menuType: 1,
                    url: '/sys/menu',
                    name: '菜单管理',
                    icon:'el-icon-menu',
                    parentId:null,
                })
            }
        }
        conditionParms = {}
    } else {
        conditionParms = { _id: { $in: menuIds } }
    }
    const res = await Menus.find({ ...selectParms, ...conditionParms }).sort({ 'orderNum': 1 })//根据orderNum排序

    if (res) {
        const menuArr = getMenuTree(res, null, [])
        ctx.body = new successModel(menuArr, "查询成功")
        return
    }
    ctx.body = new errorModel("查询失败", code.DB_ERROR)
}
//新增编辑菜单
const addMenus = async (ctx, params) => {
    const { menuType, name, url, icon, limitCode, parentId, orderNum, level, updateTime, _id, action } = params
    const body = { menuType, name, url, icon, limitCode, parentId, orderNum, level, updateTime }
    //新增
    if (action == 0) {
        try {
            //这一步运行一次就可以注释掉，自增需要有个初始值
            const res = await Counter.findOne({ id: "menuId" })
            if (!res) {
                await Counter.create({
                    "id": "menuId",
                    "sequence_value": 1
                })
            }
            const doc = await Counter.findOneAndUpdate({ id: 'menuId' }, { $inc: { sequence_value: 1 } }, { new: true })
            if (!doc) {
                ctx.body = new errorModel("新增失败", code.DB_ERROR)
                return
            }
            await Menus.create({ id: doc.sequence_value, ...body })
            ctx.body = new successModel("新增成功")
        } catch (error) {
            ctx.body = new errorModel("新增失败", code.DB_ERROR)
        }
        return
    }
    //编辑
    try {
        const res = await Menus.findOneAndUpdate({ _id }, body)
        if (!res) {
            ctx.body = new errorModel("更新失败", code.DB_ERROR)
            return
        }
        ctx.body = new successModel("更新成功")
    } catch (error) {
        ctx.body = new errorModel("更新失败", code.DB_ERROR)
    }
}
//删除菜单
const delMenuList = async (ctx, _id) => {
    const res = await Menus.find()
    const _ids = delMenuTree(res, _id, [])
    const delIts = await Menus.deleteMany({ _id: _ids })
    if (delIts) {
        ctx.body = new successModel(`删除成功${delIts.deletedCount}条`)
        return
    }
    ctx.body = new errorModel("删除失败", code.DB_ERROR)
}
module.exports = { queryMenus, addMenus, delMenuList }
/**
 *
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　　　　　┃
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　　　　　┃
 * 　　┃　　　┻　　　┃
 * 　　┃　　　　　　　┃
 * 　　┗━┓　　　┏━┛Code is far away from bug with the animal protecting
 * 　　　　┃　　　┃    神兽保佑,代码无bug
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　 ┣┓
 * 　　　　┃　　　　 ┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 *
 */